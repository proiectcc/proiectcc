CREATE TABLE IF NOT EXISTS movies (
    id serial PRIMARY KEY,
    title varchar NOT NULL,
    rating varchar NOT NULL,
    genre varchar NOT NULL,
    seen varchar NOT NULL
);