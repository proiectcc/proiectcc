# Watchlist API
## Maintainers
* Victor Diaconu
* Stefan Olarescu
* Andrei Smarandoiu

## Setup environment
```
docker swarm init --advertise-addr <ip>

# Generate SSH keys from remote host (optional)
ssh-keygen
cat ~/.ssh/id_rsa | pbcopy
paste on local host

# Add secrets admin
echo "admin" | docker secret create imdbv2-db-user-secret -
echo "admin" | docker secret create imdbv2-db-password-secret -

# Copy docker-compose on remote host
scp docker-compose-swarm.yml <ssh_address>:.
scp -r Database <ssh_address>:.
scp stack-portainer.yml <ssh_address>:.
scp -r kong <ssh_address>:.

# Run docker compose
docker login registry.gitlab.com
docker stack deploy --with-registry-auth -c docker-compose-swarm.yml imdbv2

# Deploy Portainer
docker stack deploy --compose-file=stack-portainer.yml imdbv2

# GITLAB commands
mkdir config

docker run -d --name gitlab-runner --restart always \
     -v /Users/smara/University/master/CC/IMDBv2/apigateway/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest

docker run --rm -it -v /Users/smara/University/master/CC/IMDBv2/apigateway/config:/etc/gitlab-runner gitlab/gitlab-runner register

[update privileged -> true and mouting volumes by adding "/var/run/docker.sock:/var/run/docker.sock"]

docker restart gitlab-runner
```

## Legend
* Obtain the jwt token to access a user's specific watchlist
```
POST  /api/login   
{
	"username": "[USERNAME]"
}
```

* Get the logged in user's specific watchlist
```
GET   /api/watchlist
Bearer Token: "[TOKEN]"
```

* Get details from a specific movie using the id from the previous request
```
GET   /api/watchlist/[ID]
Bearer Token: "[TOKEN]"
```

* Get the movies that have already been watched
```
GET   /api/watchlist/seen
Bearer Token: "[TOKEN]"
```

* Add a movie the the watchlist
```
POST  /api/watchlist
{
    "title": "Star Wars",
    "rating": "10",
    "genre": "SF"
}
Bearer Token: "[TOKEN]"
```

* Mark a movie as watched
```
POST  /api/watchlist/watch
{
    "title": "[TITLE]"
}
Bearer Token: "[TOKEN]"
```