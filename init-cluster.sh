docker swarm init --advertise-addr <ip>

# Generate SSH keys from remote host (optional)
ssh-keygen
cat ~/.ssh/id_rsa | pbcopy
paste on local host

# Add secrets admin
echo "admin" | docker secret create pgpassword-v1 -
echo "admin" | docker secret create postgres_password-v1 -

# Copy docker-compose on remote host
scp docker-compose-swarm.yml <ssh_address>:.
scp -r Database <ssh_address>:.
scp stack-portainer.yml <ssh_address>:.
scp -r kong <ssh_address>:.

# Run docker compose
docker login registry.gitlab.com
docker stack deploy --with-registry-auth -c docker-compose-swarm.yml imdbv2

# Deploy Portainer
docker stack deploy --compose-file=stack-portainer.yml imdbv2

# GITLAB commands
mkdir config

docker run -d --name gitlab-runner --restart always \
     -v /Users/smara/University/master/CC/IMDBv2/apigateway/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest

docker run --rm -it -v /Users/smara/University/master/CC/IMDBv2/apigateway/config:/etc/gitlab-runner gitlab/gitlab-runner register

[update privileged -> true and mouting volumes by adding "/var/run/docker.sock:/var/run/docker.sock"]

docker restart gitlab-runner